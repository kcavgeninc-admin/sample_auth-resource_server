package com.koz;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KozAuthMongodbServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(KozAuthMongodbServerApplication.class, args);
	}
}
