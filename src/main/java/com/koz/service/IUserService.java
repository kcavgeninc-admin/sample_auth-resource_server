package com.koz.service;

import com.koz.model.User;
import com.koz.validation.EmailExistsException;

public interface IUserService {

	User findUserByEmail(final String email);

    User registerNewUser(User user) throws EmailExistsException;

//    void createVerificationTokenForUser(User user, String token);

//    VerificationToken getVerificationToken(String token);

    void saveRegisteredUser(User user);

	void save(User user);
}
