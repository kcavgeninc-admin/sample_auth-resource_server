package com.koz.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.koz.model.User;
import com.koz.repository.UserRepository;
import com.koz.validation.EmailExistsException;



@Service
public class UserService implements IUserService {

	@Autowired
    private UserRepository userRepository;
//    @Autowired
//    private VerificationTokenRepository verificationTokenRepository;
    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;
    
    public User findUserByEmail(String email) {
        return userRepository.findByEmail(email);
    }
    
    @Override
    public User registerNewUser(final User user) throws EmailExistsException {
        if (emailExist(user.getEmail())) {
            throw new EmailExistsException("There is an account with that email address: " + user.getEmail());
        }
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        return userRepository.save(user);
    }

//    @Override
//    public void createVerificationTokenForUser(final User user, final String token) {
//        final VerificationToken myToken = new VerificationToken(token, user);
//        verificationTokenRepository.save(myToken);
//    }
//
//    @Override
//    public VerificationToken getVerificationToken(final String token) {
//        return verificationTokenRepository.findByToken(token);
//    }
    
    @Override
    public void saveRegisteredUser(final User user) {
    	user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        userRepository.save(user);
    }
    
    private boolean emailExist(final String email) {
        final User user = userRepository.findByEmail(email);
        return user != null;
    }

	@Override
	public void save(User user) {
		user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        userRepository.save(user);
		
	}
}
