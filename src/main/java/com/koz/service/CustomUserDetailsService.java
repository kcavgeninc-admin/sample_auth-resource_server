package com.koz.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;


import com.koz.model.Role;
import com.koz.model.User;
import com.koz.repository.RoleRepository;
import com.koz.repository.UserRepository;



/**
*
* @author Koz
*/
public class CustomUserDetailsService implements UserDetailsService {
	
	
//   @Autowired
//   private UserRepository userRepository;
   @Autowired
   private RoleRepository roleRepository;
   @Autowired
   private IUserService userService;
   @Autowired
   private BCryptPasswordEncoder bCryptPasswordEncoder;
   
   public CustomUserDetailsService() {
       super();
   }
   
   public User findUserByEmail(String email) {
       return userService.findUserByEmail(email);
   }

   public void saveUser(User user) {
       user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
       user.setEnabled(true);
       Role userRole = roleRepository.findByRole("ADMIN");
       user.setRoles(new HashSet<>(Arrays.asList(userRole)));
       userService.save(user);
   }
   
//   @Override
//   public UserDetails loadUserByUsername(final String email) throws UsernameNotFoundException {
//       try {
//           final User user = userService.findUserByEmail(email);
//           if (user == null) {
//               throw new UsernameNotFoundException("No user found with email: " + email);
//           }
//
//           return new org.springframework.security.core.userdetails.User(user.getEmail(), user.getPassword(), true, true, true, true, Arrays.asList(new SimpleGrantedAuthority("ROLE_USER")));
//       } catch (final Exception e) {
//           throw new RuntimeException(e);
//       }
//   }

   @Override
   public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {

       User user = userService.findUserByEmail(email);  
       if(user != null) {
           List<GrantedAuthority> authorities = getUserAuthority(user.getRoles());
           return buildUserForAuthentication(user, authorities);
       } else {
           throw new UsernameNotFoundException("username not found");
       }
   }

   private List<GrantedAuthority> getUserAuthority(Set<Role> userRoles) {
       Set<GrantedAuthority> roles = new HashSet<>();
       userRoles.forEach((role) -> {
           roles.add(new SimpleGrantedAuthority(role.getRole()));
       });

       List<GrantedAuthority> grantedAuthorities = new ArrayList<>(roles);
       return grantedAuthorities;
   }

   private UserDetails buildUserForAuthentication(User user, List<GrantedAuthority> authorities) {
       return new org.springframework.security.core.userdetails.User(user.getEmail(), user.getPassword(), authorities);
   }

   
}
