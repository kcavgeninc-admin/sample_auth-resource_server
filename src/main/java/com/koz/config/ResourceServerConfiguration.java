package com.koz.config;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.provider.token.RemoteTokenServices;
import org.springframework.security.oauth2.provider.token.ResourceServerTokenServices;
import org.springframework.security.web.header.HeaderWriter;

import com.koz.service.CustomUserDetailsService;

@Configuration
@EnableResourceServer
//@EnableWebSecurity
//@EnableGlobalMethodSecurity(securedEnabled = true)
public class ResourceServerConfiguration extends ResourceServerConfigurerAdapter {

	/*
//    @Autowired
//    private UserDetailsService userDetailsService;
    
    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Value("${signing-key:oui214hmui23o4hm1pui3o2hp4m1o3h2m1o43}")
    private String signingKey;

    public ResourceServerConfiguration() {
        super();
    }

    // global security concerns

//    @Bean
//    public AuthenticationProvider authProvider() {
//        final DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
//        authProvider.setUserDetailsService(userDetailsService);
//        return authProvider;
//    }
    
    @Bean
    public UserDetailsService mongoUserDetails() {
        return new CustomUserDetailsService();
    }

    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder(12);
        return bCryptPasswordEncoder;
    }
    
    @Autowired
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        UserDetailsService userDetailsService = mongoUserDetails();
        auth
                .userDetailsService(userDetailsService)
                .passwordEncoder(bCryptPasswordEncoder);

    }

//    @Autowired
//    public void configureGlobal(final AuthenticationManagerBuilder auth) {
//        auth.authenticationProvider(authProvider());
//    }

    // http security concerns

    @Override
    public void configure(final HttpSecurity http) throws Exception {
        // @formatter:off
        http.
        authorizeRequests().
        // antMatchers("/oauth/token").permitAll().
        anyRequest().authenticated().and().
        sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and().
        csrf().disable();
        // @formatter:on
    }
*/
//	@Bean
//    public BCryptPasswordEncoder passwordEncoder() {
//        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder(12);
//        return bCryptPasswordEncoder;
//    }
	
	@Override
    public void configure(HttpSecurity http) throws Exception {// @formatter:off
        http
        .requestMatchers().antMatchers("/api/users/**")
        .and()
        .authorizeRequests()
        .antMatchers(HttpMethod.GET,"/api/users/**").access("#oauth2.hasScope('read')")
        .antMatchers(HttpMethod.POST,"/api/users/**").access("#oauth2.hasScope('write')")
        .antMatchers(HttpMethod.DELETE,"/api/users/**").access("#oauth2.hasScope('write')")
        ;
    }// @formatter:on       
	
//	@Override
//    public void configure(HttpSecurity http) throws Exception {
//        http
//                .authorizeRequests()
//                .antMatchers(HttpMethod.GET, "/**").access("#oauth2.hasScope('read-foo')")
//                .and()
//                .headers().addHeaderWriter(new HeaderWriter() {
//            @Override
//            public void writeHeaders(HttpServletRequest request, HttpServletResponse response) {
//                response.addHeader("Access-Control-Allow-Origin", "*");
//                if (request.getMethod().equals("OPTIONS")) {
//                    response.setHeader("Access-Control-Allow-Methods", request.getHeader("Access-Control-Request-Method"));
//                    response.setHeader("Access-Control-Allow-Headers", request.getHeader("Access-Control-Request-Headers"));
//                }
//            }
//        });
//    }
        

//    @Bean
//    public ResourceServerTokenServices tokenService() {
//       RemoteTokenServices tokenServices = new RemoteTokenServices();
//       tokenServices.setClientId("lssClient");
//       tokenServices.setClientSecret("lssSecret");       
//       tokenServices.setCheckTokenEndpointUrl("http://localhost:8553/um-webapp-auth-server/oauth/check_token");
//       return tokenServices;
//    }
}