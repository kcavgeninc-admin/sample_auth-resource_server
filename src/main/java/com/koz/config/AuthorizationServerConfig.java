package com.koz.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
//import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.InMemoryTokenStore;

import com.koz.service.CustomUserDetailsService;

@Configuration
@EnableAuthorizationServer
public class AuthorizationServerConfig extends AuthorizationServerConfigurerAdapter {

	@Autowired
	private AuthenticationManager authenticationManager;
//	
//	@Autowired
//    private UserDetailsService userDetailsService;

//	@Autowired
//    CustomizeAuthenticationSuccessHandler customizeAuthenticationSuccessHandler;

//	@Autowired
//	private TokenStore tokenStore;

	public AuthorizationServerConfig() {
		super();
	}

//    // beans
//    @Bean
//    public UserDetailsService mongoUserDetails() {
//        return new CustomUserDetailsService();
//    }
//    @Bean
//    public TokenStore tokenStore() {
//        return new InMemoryTokenStore();
//    }

	@Override
	public void configure(final ClientDetailsServiceConfigurer clients) throws Exception {// @formatter:off
		clients.inMemory()
        .withClient("lssClient")
        .secret("lssSecret")
        .authorizedGrantTypes("authorization_code", "refresh_token")
        .scopes("read","write")
//                   .autoApprove(true)
		;
//                   .refreshTokenValiditySeconds(3600 * 24)
//                   .accessTokenValiditySeconds(3600);
		// @formatter:on
	}

//	@Override
//    public void configure(final AuthorizationServerEndpointsConfigurer endpoints) {// @formatter:off
////		UserDetailsService userDetailsService = mongoUserDetails();
//        endpoints.
//        tokenStore(tokenStore).
//        authenticationManager(authenticationManager).
//        userDetailsService(userDetailsService).
//        allowedTokenEndpointRequestMethods(HttpMethod.GET, HttpMethod.POST);
//    }// @formatter:on
//	
//	@Override
//	public void configure(AuthorizationServerSecurityConfigurer oauthServer) throws Exception {
//		oauthServer.tokenKeyAccess("permitAll()").checkTokenAccess("isAuthenticated()")
//				.allowFormAuthenticationForClients();
////        oauthServer.checkTokenAccess("permitAll()"); 
//	}
//
//	@Override
//	public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
//		endpoints.prefix("/servlet").authenticationManager(authenticationManager).allowedTokenEndpointRequestMethods(HttpMethod.GET,
//				HttpMethod.POST);
//		;
//	}

//	@Override
//	public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
//		endpoints.prefix("/servlet").authenticationManager(authenticationManager);
//	}

//	@Override
//    public void configure(final AuthorizationServerEndpointsConfigurer endpoints) {// @formatter:off
//        final TokenEnhancerChain tokenEnhancerChain = new TokenEnhancerChain();
//        tokenEnhancerChain.setTokenEnhancers(Arrays.asList(tokenEnhancer(), accessTokenConverter()));
//        
//        endpoints.
//        tokenStore(tokenStore()).
//        tokenEnhancer(tokenEnhancerChain). // new
//        authenticationManager(authenticationManager).
//        userDetailsService(userDetailsService).
//        allowedTokenEndpointRequestMethods(HttpMethod.GET, HttpMethod.POST).
//        accessTokenConverter(accessTokenConverter());
//    }// @formatter:on
//
//    @Override
//    public void configure(AuthorizationServerSecurityConfigurer security) throws Exception {
//        security.checkTokenAccess("permitAll()");
//        super.configure(security);
//    }
//
//    @Bean
//    public TokenEnhancer tokenEnhancer() {
//        return new CustomTokenEnhancer();
//    }

}
