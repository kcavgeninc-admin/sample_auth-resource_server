package com.koz.repository;

import java.math.BigInteger;
import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.koz.model.User;



public interface UserRepository extends CrudRepository<User, String> {
    
    User findByEmail(String email);
    
    void deleteById(BigInteger id);
    
    public List<User> findAll();
    
}
